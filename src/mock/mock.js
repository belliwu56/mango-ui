import Mock from 'mockjs'

Mock.mock('http://localhost:8001/login', {
    'token': '332fr3e3rfsdfd'
})

Mock.mock('http://localhost:8080/user', {
    'name': '@name',  //隨機生成姓名
    'name': '@email',  //隨機生成郵箱
    'age|1-10': 5,  //年齡 1-10 之間
})

Mock.mock('http://localhost:8080/menu', {
    'id': '@increment',  // id 自增
    'name': 'menu',  //名稱為 menu
    'order|1-20': 5,  //排序 1-20之間
})
