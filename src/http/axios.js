import axios from 'axios';
import config from './config';
import qs from 'qs';
import Cookies from "js-cookie";
import router from '@/router'

// 使用 vuex 做全局 loading 時使用
// import store from '@/store' 

export default function $axios(options)
{
    return new Promise((resolve, reject) =>
    {
        //1. instance declare
        const instance = axios.create({
            baseURL: config.baseURL,
            headers: config.headers,
            timeout: config.timeout,
            withCredentials: config.withCredentials
            // transformResponse: [function (data) { }]
        });

        // 2. request 攔截器
        instance.interceptors.request.use(
            config =>
            {
                let token = Cookies.get('token');

                // A. 請求開始的時候，可以結合 vuex 開啟全屏 loading 動畫
                // console.log(store.state.loading) 
                // console.log('準備發送請求.. .') 

                // B. 帶上token 
                if (token) config.headers.accessToken = token;
                else // 重定向到登錄頁面 
                    router.push('/login')

                return config;
            }, //End for config()

            error =>
            {
                return Promise.reject(error);
            } //End for error()
        );

        // 3. response 攔截器
        instance.interceptors.response.use(
            response =>
            {
                return response.data;
            },
            error =>
            {
                return Promise.reject(error);
            }//End for error()
        );//End for instance.interceptors.response

        //4. Request 處理
        instance(options).then(resp =>
        {
            resolve(resp);
            return false;
        }).catch(error =>
        {
            reject(error);
        })

    }  //End for function (resolve, reject){}
    );//End for Promise()
}//End for $axios(options)
