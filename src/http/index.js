// 導入所有 api 介面
import api from './api'

const install = Vue =>
{
    if (install.installed) return;
    install.installed = true;

    Object.defineProperties(Vue.prototype, {
        // 注意，此處掛載在 Vue 原型的$ api 物件上
        $api: { get() { return api } }
    })
}

export default install;
