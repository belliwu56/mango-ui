import { baseUrl } from '@/utils/global'

export default {
    method: "get",

    // 基礎 url 前綴 
    baseURL: baseUrl,

    // 請求頭信息
    headers: {
        'Content-Type': 'application/json; charset=UTF-8'
    },

    // 參數
    data: {},

    // 設置超時時間 
    timeout: 10000,

    // 攜帶憑證 
    withCredentials: true,

    // 返回數據類型 
    responseType: 'json'
}
