import axios from '../axios'
/*
 * 用戶管理模塊
 */

// 保存
export const save = (data) =>
{
    return axios({
        url: '/user/save',
        method: 'post',
        data
    })
}

// 刪除
export const batchDelete = (data) =>
{
    return axios({
        url: '/user/delete',
        method: 'post',
        data
    })
}

// 分頁查詢
export const findPage = (data) =>
{
    return axios({
        url: '/user/findPage',
        method: 'post',
        data
    })
}
// 查找用戶的菜單權限標識集合
export const findPermissions = (params) =>
{
    return axios({
        url: '/user/findPermissions',
        method: 'get',
        params
    })
}

