import axios from './axios'
/*
 * 將所有介面統一起來便於維護
 * 如果專案很大可以將 url 獨立成文件，介面分成不同的模塊
 */

// 單獨導出
export const login = () =>
{
    return axios({
        url: '/login',
        method: 'get'
    })
}

export const getUser = () =>
{
    return axios({
        url: '/login',
        method: 'get'
    })
}

export const getMenu = data =>
{
    return axios({
        url: '/menu',
        method: 'post',
        data
    })
}

// 預設全部導出 
export default {
    login,
    getUser,
    getMenu
}
